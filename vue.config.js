module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160291/learn_bootstrap/'
    : '/'
}
